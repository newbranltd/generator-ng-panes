;(function()
{
    'use strict';

    /**
     * @ngdoc directive
     * @name <%= scriptAppName %>.directive:<%= cameledName %>
     * @description
     * # <%= cameledName %>
     */
    angular.module('<%= scriptAppName %>').directive('<%= cameledName %>', function()
    {
        <% if (!externalTemplate && !slim) { %>
        // define your template
        var <%= cameledName %>Tpl = '<div></div>';
        <% } %>
        <% if (!slim) { %>
        // define your controller
        var <%= cameledName %>DirectiveCtrl = function($scope)
        {
            var vm = this;
        };<% } %>
        // export
        return {
            <% if (externalTemplate && !slim) { %>templateUrl: '<%= externalTemplate %>',<% } else if (!externalTemplate && !slim) { %>template: <%= cameledName %>Tpl,<% } %>
            restrict: '<%= restrictValue %>',
            // replace: true, // uncomment this if you want to get rip of the <<%= cameledName %>>
            // transclude: true, // uncomment if you are using transclude 
            scope: {},
            <% if (!slim) { %>controller: <%= cameledName %>DirectiveCtrl, // ngAnnotate will do the injection
            controllerAs: 'vm',
            // don't use the scope, use bindToController instead so we can call them like vm.variable
    		// bindToController: {},
            <% } %>link: function(scope, element, attrs)
            {
                <% if (!slim) { %>element.text('this is the <%= cameledName %> directive');<% } %>
            }
        };
    });
}());
