;(function()
{
    'use strict';
    /**
     * @ngdoc overview
     * @name <%= scriptAppName %>
     * @description
     * # <%= scriptAppName %>
     *
     * Main module of the application.
     */
    /* ngModuleStart */
    angular.module('<%= scriptAppName %>', [<%- angularModules %>])
    /* ngModuleEnd */
    <% if (ngRoute=='angular-route') { %>
    .config(function($routeProvider)
    {
        $routeProvider
        .when('/', {
            template: '<site-nav></site-nav><main></main>'
        })
        .otherwise({
            redirectTo: '/'
        });

    })<% } else { %>
    .config(function($stateProvider , $urlRouterProvider)
    {

        $stateProvider
        .state('index' , {
            url: '/',
            template: '<site-nav></site-nav><main></main>'
        });

        $urlRouterProvider.otherwise('/');

    })<% } %>;
    // also provide a appController here, althought its not recommended to put anything in the $rootScope
    /*
    angular.module('<%= scriptAppName %>').run(function($rootScope)
    {
        // do your thing here
    });
    */
    //Then init the app
    angular.element(document).ready(function()
    {
    	angular.bootstrap(document, ['<%= scriptAppName %>'] , {strictDi: true});

        <% if (uiframework==='bootstrapMaterialDesign') { %>
        // init the js script for material design
        $.material.init();
        <% } %>
    });


}());
