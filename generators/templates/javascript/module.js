;(function()
{
	'use strict';
	/**
	 * a standalone module <%= moduleName %>
	 */
	var <%= cameledName %>Module = angular.module('<%= moduleName %>', []);
	/*
	this is commented out because you might not want to define your sub route.

	<% if (ngRoute=='angular-route') { %>
	<%= cameledName %>Module.config(function($routeProvider)
	{
		// define your sub route

		// .otherwise({redirectTo: '/'});

	<% } else { %>
	<%= cameledName %>Module.config(function($stateProvider)
	{
		// define your sub route

	<% } %>});
	*/
}());
