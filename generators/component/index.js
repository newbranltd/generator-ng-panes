'use strict';
/**
 * this is a Angular 1.5 only features
 */
var util = require('util');
var path = require('path');
var chalk = require('chalk');
var _ = require('underscore');
var ScriptBase = require('../../lib/script-base.js');
var preference = require('../../lib/preference');
var engine       = require('../../lib/engines').underscore;

// this is coming from the yeoman-generator inside the generator-karma - don't even ask how that's possible
var _engine = function (body, data, options)
{
    return engine.detect(body) ? engine(body, data, options) : body;
};

/**
    create a new component for ng v.1.5
**/
var isAvailable = function()
{
    var bower = require(path.join(process.cwd() , 'bower.json'));
    return (bower.dependencies.angular.indexOf('1.5') > -1);
};

/**
 * Constructor
 */
var Generator = module.exports = function()
{
     ScriptBase.apply(this, arguments);

     this.config = preference.getConfig();

     var notpl = (this.env.options.notpl || this.options.notpl);
     // use external file
     this.externalTemplate = (notpl) ? false : true;

     this.whatType = 'Component';

};

util.inherits(Generator, ScriptBase);

/**
 * generate the component files
 */
Generator.prototype.createDirectiveFiles = function()
{
    this.dasherizeName = _.dasherize(this.name);

    this.cssClassName = this.dasherizeName + '-' + this.whatType.toLowerCase();

    var moduleDir = this.checkModuleOption();

    if (this.externalTemplate !== false) {
        this.externalTemplate = (moduleDir!=='') ? path.join('scripts' , 'modules' , moduleDir , 'views' , 'components' , this.dasherizeName + '.html')
                                                 : path.join('views' , 'components' , this.dasherizeName + '.html');

        this.externalTemplate = this.fixPath(this.externalTemplate);
    }

    this.generateSourceAndTest(
        'component',
        'spec/component',
        'components',
        moduleDir
    );

    var filePath = path.join('..', 'common' , 'app' , 'views' , 'directive.html');

    // generate external file
    if (this.externalTemplate !== false) {
        if (this.options.uiframework) {

            this.ngRouteTag = this.options.ngRouteTag;

            this.overwrite = _engine(
                this.read(
                    path.join('module' , this.options.uiframework , this.dasherizeName + '.html')
                ), this);
            this.template(
                filePath,
                path.join(this.env.options.appPath , this.externalTemplate)
            );
        }
        else {
            this.overwrite = false;
            this.htmlTemplate(
                filePath,
                this.externalTemplate
            );
        }
    }
};

// -- EOF --
